# Epidemic simulation

Simulation of number of cases.

## Warning about the results given by the simulation

The simulation is not based on academic paper and not validated by fields data. This is a personal works to help to understand the spead of the disease.

All the parameters of the simulation are fixed with my impression about the disease and are not fixed by a scientific research.

So the results are not correct and do not predict the spead of the disease. The idea is to try to understand how works the epidemic.

## About the simulation

Two scenarii are tested. These two scenarii have an Re factor of 3 (which was a Re-factor given at the begining of the epidemic in Belgium). In the simulation we investigate the idea of visible case : case that are detected. For the moment all the infected person become visible when the symfoms appears.

The first scenario (called `sce1` or `basic`) is simple : when a person is ill, each day during the disease he has a propability to infect another one.

The second scenario (called `scr2` or `family_prop`) is a bit more complexe : we estimate that each ill person contamin all its family at least the day when the symptoms appears. With the idea of comparing the scenarii, we adapt the propability of infection to keep at 3 the Re factor.

## Results

The results are visible in the `results` repository.

What I found interesting is :

- for the two scenarii there exist a period where the epidemic is unvisible (this is obvious and predictable but interesting to see)
- the second scenario is more virulent than the first one (this is also obvious and predictable but interesting to see)

## Future work

We would like to investigate :

- asymptomatic cases
- quarantine
- stragegy of testing (to detect unvisible cases)

## License (MIT)

See `LICENSE` file.

