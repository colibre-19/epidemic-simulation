import numpy as np
import matplotlib.pyplot as plt
import csv


class Simulation:
    """
        The simulator
    """
    def __init__(self):
        self.disease = None
        self.population = None

class Disease:
    """
        The disease.

        Parameters:
            simu (Simulation): the simulation
            duration (integer): the number of days of the disease, i.e. during which the patient is contagious
            symptoms_day (integer): the day when the symptoms appears (0 = day of the infection)
            contagion_factors (list of floats): indicates for each day the number of person that an ill person transmits the disease (example : [0, 0.5] means that day 0 (contagion) the ill person does not transmit the disease and that day 1 he will transmit the disease to 0.5 (50% percent of change to transmit to another persone))
    """
    def __init__(self, simu, duration, symptoms_day, contagion_factors):
        if len(contagion_factors) != duration:
            raise ValueError(f'Length of contagion_factors ({len(contagion_factors)}) must be duration minus 1 ({duration})')

        self.simu = simu
        simu.disease = self
        self.contagion_factors = contagion_factors
        self.symptoms_day = symptoms_day
        self.duration = duration


    def stats(self):
        print('DISEASE STATS')
        print(f'(D) Number of person that an ill person will infect (Re) : {sum(self.contagion_factors)}')


class Population:
    """
        The population.

        Parameters:
            simu (Simulation): the simulation
            size (int): size of the population
            family_size (float): number of person that lives in the same house
            infected_cases_nb (int): number of infected people at the first day of the population
            daily_test_nb (int): number of person that can be tested (if they have the disease or not)
        """
    def __init__(self, simu, size, family_size, infected_cases_nb, daily_test_nb):
        self.simu = simu
        simu.population = self
        self.size = size
        self.family_size = family_size
        self.infected_cases_nb = infected_cases_nb
        self.daily_test_nb = daily_test_nb

    def stats(self):
        print('COUNTRY STATS')
        print(f'(C) % infected cases : {self.infected_cases_nb / self.size}')
        print(f'(C) prob daily test  : {self.daily_test_nb / self.size}')


class AbstractScenario:
    def __init__(self, simu, name):
        self.simu = simu
        self.name = name
        self.infected_cases_by_day = []
        self.visible_cases_by_day = []
        self.recovered_cases_by_day = []
        self.current_day = 0

    def compute_next_day(self):
        """
        Compute the cases for the next day
        """
        raise NotImplementedError()

    def current_day_visible_cases(self):
        """
        Infected cases that are visibles for the current day

        Returns int
        """
        raise NotImplementedError()


    def current_day_infected_cases(self):
        """
        All the Infected cases for the current day

        Returns int
        """
        raise NotImplementedError()

    def current_day_recovered(self):
        """
        All the recovered for the current day

        Returns int
        """
        raise NotImplementedError()

    def run(self, number_of_days):
        """ Executer le scenario pour un certain nombre de jours"""
        for _ in range(number_of_days):
            self.infected_cases_by_day.append(self.current_day_infected_cases())
            self.visible_cases_by_day.append(self.current_day_visible_cases())
            self.recovered_cases_by_day.append(self.current_day_recovered())

            self.compute_next_day()
            self.current_day += 1


class Scenario(AbstractScenario):
    """
        Simple Scenario of the propagation of the disease.

        Parameters:
            simu (Simulation): the simulation
    """
    def __init__(self, simu, name):
        AbstractScenario.__init__(self, simu, name)

        self.infected_cases = np.array([0.] * self.simu.disease.duration) # number of persons infected (day by day)
        self.infected_cases[0] =  self.simu.population.infected_cases_nb # the infected persons at the begin
        self.recovered = 0

    def compute_next_day(self):
        """
        Actualize the data for a new day

        """
        self.recovered = self.recovered + self.infected_cases[-1]

        # switch des datas
        for i in range(1, len(self.infected_cases)):
            self.infected_cases[-i] = self.infected_cases[-(i + 1)]


        new_infected_case = 0

        for i in range(1, self.simu.disease.duration):
            # quand par externe on contamine 1 fois son foyer (sinon au sein de la familly elle a déjà été)

            new_infected_case = new_infected_case + self.infected_cases[i] * self.simu.disease.contagion_factors[i] # chaque malade contamine des gens externes

        self.infected_cases[0] = new_infected_case


    def current_day_infected_cases(self):
        return int(sum(self.infected_cases))

    def current_day_visible_cases(self):
        return int(sum(self.infected_cases[self.simu.disease.symptoms_day:]))
         
    def current_day_recovered(self):
        return int(self.recovered)


class Scenario2(AbstractScenario):
    """
        Scenario of the propagation of the disease. This scenario simulate the fact that all the people living in the same house becames all infected the day when the symptoms appears

        Parameters:
            simu (Simulation): the simulation
    """
    def __init__(self, simu, name):
        AbstractScenario.__init__(self, simu, name)

        self.infected_cases_by_ext = np.array([0.] * self.simu.disease.duration) # number of persons infected (day by day) by an external person (person not living in the same house)
        self.infected_cases_by_fam = np.array([0.] * self.simu.disease.duration) # number of persons infected (day by day) inside the family (person living in the same house)
        self.infected_cases_by_ext[0] =  self.simu.population.infected_cases_nb # the infected persons at the begin
        self.recovered = 0
        self.current_day = 0

    def compute_next_day(self):
        """
        Actualize the data for a new day

        """
        self.current_day += 1
        self.recovered = self.recovered + self.infected_cases_by_ext[-1] + self.infected_cases_by_fam[-1]

        # switch des datas
        for i in range(1, len(self.infected_cases_by_ext)):
            self.infected_cases_by_ext[-i] = self.infected_cases_by_ext[-(i + 1)]
            self.infected_cases_by_fam[-i] = self.infected_cases_by_fam[-(i + 1)]


        self.infected_cases_by_ext[0] = 0
        self.infected_cases_by_fam[0] = 0

        new_infected_cases_by_ext = 0

        for i in range(1, self.simu.disease.duration):
            new_infected_cases_by_ext = new_infected_cases_by_ext \
                + (self.infected_cases_by_ext[i] + self.infected_cases_by_fam[i]) * self.simu.disease.contagion_factors[i] # chaque malade contamine des gens externes

        self.infected_cases_by_ext[0] = new_infected_cases_by_ext 


        new_infected_cases_by_fam = self.infected_cases_by_ext[self.simu.disease.symptoms_day] * (self.simu.population.family_size - 1) # person infected by extern will infect his family
        self.infected_cases_by_fam[0] = new_infected_cases_by_fam


    def current_day_infected_cases(self):
        return int(sum(self.infected_cases_by_ext) + sum(self.infected_cases_by_fam))

    def current_day_visible_cases(self):
        return int(sum(self.infected_cases_by_ext[self.simu.disease.symptoms_day:]) + sum(self.infected_cases_by_fam[self.simu.disease.symptoms_day:]))
         
    def current_day_recovered(self):
        return int(self.recovered)


if __name__ == '__main__':
    days_of_simulation = 75

    contagion_factors = [0, 0, 0, 0, 0.1, 0.2, 0.3, 0.4, 0.3, 0.3, 0.3, 0.3, 0.2, 0.2, 0.2, 0.1, 0.1, 0]

    simu = Simulation()

    belgium = Population(simu, 11460000, 2.5, 1, 5000)

    belgium.stats()

    corona = Disease(simu, 18, 7, contagion_factors)
    corona.stats()

    scr1 = Scenario(simu, 'basic')
    scr1.run(days_of_simulation)


    scr2 = Scenario2(simu, 'family_prop')

    # this scenario increase by one the Re number
    correction_factor = (sum(corona.contagion_factors) - 1) / sum(corona.contagion_factors)
    print(correction_factor)

    corona.contagion_factors = [x * correction_factor for x in corona.contagion_factors]
    corona.stats()

    scr2.run(days_of_simulation)

    plt.plot(scr1.visible_cases_by_day, 'b', label='basic')
    plt.plot(scr2.visible_cases_by_day, 'r', label='with family contagion')
    
    plt.ylabel('Visible cases')
    plt.xlabel('Days')
    plt.legend()
    # plt.show()
    plt.savefig(f'./results/comp_{scr1.name}_{scr2.name}.png')

    plt.clf() # clear
    plt.plot(scr1.infected_cases_by_day, 'r', label='infected')
    plt.plot(scr1.visible_cases_by_day, 'b', label='visible')
    plt.plot(scr1.recovered_cases_by_day, 'g', label='recovered')
        
    plt.ylabel('Number of cases')
    plt.xlabel('Days')
    plt.legend()
    # plt.show()
    plt.savefig(f'./results/scr_{scr1.name}.png')


    plt.clf() # clear
    plt.plot(scr2.infected_cases_by_day, 'r', label='infected')
    plt.plot(scr2.visible_cases_by_day, 'b', label='visible')
    plt.plot(scr2.recovered_cases_by_day, 'g', label='recovered')
        
    plt.ylabel('Number of cases')
    plt.xlabel('Days')
    plt.legend()
    # plt.show()
    plt.savefig(f'./results/scr_{scr2.name}.png')


    with open(f'./results/comp_{scr1.name}_{scr2.name}.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['Days'] + list(range(days_of_simulation)))
        writer.writerow(['scr1 inf'] + scr1.infected_cases_by_day)
        writer.writerow(['scr2 inf'] + scr2.infected_cases_by_day)
        writer.writerow([])
        writer.writerow(['scr1 visi'] + scr1.visible_cases_by_day)
        writer.writerow(['scr2 visi'] + scr2.visible_cases_by_day)
        writer.writerow([])
        writer.writerow(['scr1 recov'] + scr1.recovered_cases_by_day)
        writer.writerow(['scr2 recov'] + scr2.recovered_cases_by_day)
